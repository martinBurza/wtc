/**
 * Created by U�ivatel on 22.8.2017.
 */

// Name of localStorage ticking object
const WTC_TICKING_COUNTER = 'wtc_ticking_counter';

const LOADING_GIF = $('<img src="img/loading.gif" style="width: 25px; height: 25px;">');

const DATEPICKER_OPTIONS = {
    todayBtn: true,
    todayHighlight: true,
    autoclose: true,
    format: 'dd.mm.yyyy'
};